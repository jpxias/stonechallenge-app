# Requisitos

>**Nota**: Configure corretamente o ambiente para o React Native no link: [React Native - Environment Setup](https://reactnative.dev/docs/environment-setup)

## Passo 1: Rode o Metro Server

```bash
npm start

```

## Passo 2: Rode a aplicação


### Para Android

```bash
npm run android

```


## Passo 3: Versões utilzadas

### Node
```bash
20.11.0

```

### Java JDK
```bash
17.0.9

```


