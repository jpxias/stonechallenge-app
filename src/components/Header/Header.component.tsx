import React from 'react';
import {TouchableOpacity} from 'react-native';
import {Header as HeaderRNE, Icon} from '@rneui/themed';
import {HeaderStyles} from './Header.styles';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';

type HeaderComponentProps = {
  title: string;
  goBack: () => void;
};

const Header: React.FunctionComponent<HeaderComponentProps> = ({
  title,
  goBack,
}) => {
  return (
    <HeaderRNE
      leftComponent={
        <TouchableOpacity onPress={goBack}>
          <Icon name="arrow-left" color="white" />
        </TouchableOpacity>
      }
      centerComponent={{text: title, style: HeaderStyles.heading}}
    />
  );
};

export default Header;
