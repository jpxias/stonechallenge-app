import React from 'react';
import {Input, lightColors} from '@rneui/themed';
import {Control, Controller, FieldErrors} from 'react-hook-form';
import {View} from 'react-native';

type FormItemComponentProps = {
  name: string;
  title: string;
  control: Control<any>;
  errors: FieldErrors;
  required?: boolean;
  rules?: any;
  password?: boolean;
};

const FormItem: React.FunctionComponent<FormItemComponentProps> = ({
  name,
  title,
  control,
  errors,
  required,
  rules,
  password,
}) => {
  let allRules: any = {};

  if (required) allRules.required = {message: 'Campo obrigatório', value: true};

  allRules = {...allRules, ...rules};

  return (
    <View>
      <Controller
        control={control}
        rules={allRules}
        render={({field: {onChange, onBlur, value}}) => (
          <Input
            placeholder={title}
            onBlur={onBlur}
            onChangeText={onChange}
            secureTextEntry={password}
            value={value}
            errorStyle={{color: lightColors.error}}
            errorMessage={errors[name]?.message?.toString()}
            renderErrorMessage={!!errors[name]}
          />
        )}
        name={name}
      />
    </View>
  );
};

export default FormItem;
