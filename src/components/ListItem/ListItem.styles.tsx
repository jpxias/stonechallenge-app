import {StyleSheet} from 'react-native';

export const ListItemStyles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    margin: 5,
    borderRadius: 5,
    backgroundColor: 'whitesmoke',
    justifyContent: 'space-between',
  },

  image: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
  },

  title: {
    color: 'black',
  },

  textContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
