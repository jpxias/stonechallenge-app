import React, {PropsWithChildren} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {ListItemStyles} from './ListItem.styles';
import {tont1} from '../../../img';
import {Icon} from '@rneui/themed';

interface Item {
  id: string;
  name: string;
  serial: string;
}

type ListItemProps = PropsWithChildren<{
  item: Item;
  onDelete: () => void;
  onClick: () => void;
  showDelete: boolean;
}>;

const ListItem: React.FC<ListItemProps> = ({
  item,
  onDelete,
  onClick,
  showDelete,
}) => {
  return (
    <View key={item.id} style={ListItemStyles.container}>
      <TouchableOpacity style={ListItemStyles.textContainer} onPress={onClick}>
        <Image style={ListItemStyles.image} source={tont1}></Image>
        <View>
          <Text style={ListItemStyles.title}>{item.name}</Text>
          <Text>S/N: {item.serial}</Text>
        </View>
      </TouchableOpacity>

      {showDelete && (
        <View>
          <Icon name="delete" color="red" onPress={onDelete}></Icon>
        </View>
      )}
    </View>
  );
};

export default ListItem;
