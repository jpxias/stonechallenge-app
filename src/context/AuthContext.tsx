import {createContext, useEffect, useReducer} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ApiService from '../services/api.service';
import {AxiosInstance} from 'axios';

type StateProps = {
  isAuthenticated: boolean;
  isInitialised: boolean;
  user?: any;
};

const initialState: StateProps = {
  isAuthenticated: false,
  isInitialised: false,
  user: null,
};

type LoginInfo = {
  username: string;
  password: string;
};

type AuthProviderProps = {
  children: React.JSX.Element;
};

type ContextProps = {
  method: string;
  login: (info: LoginInfo) => Promise<void>;
  logout: () => Promise<void>;
  updateUserSession: () => Promise<void>;
  isManager: boolean;
};

const setSession = async (
  accessToken: string,
  client: AxiosInstance,
): Promise<void> => {
  if (accessToken) {
    await AsyncStorage.setItem('accessToken', accessToken);
    client.defaults.headers.Authorization = `Bearer ${accessToken}`;
  } else {
    await AsyncStorage.removeItem('accessToken');
    delete client.defaults.headers.Authorization;
  }
};

const reducer = (state: Object, action: any) => {
  switch (action.type) {
    case 'INIT': {
      const {isAuthenticated, user} = action.payload;

      return {
        ...state,
        isAuthenticated,
        isInitialised: true,
        user,
      };
    }
    case 'LOGIN': {
      const {user} = action.payload;

      return {
        ...state,
        isAuthenticated: true,
        user,
      };
    }
    case 'LOGOUT': {
      return {
        ...state,
        isAuthenticated: false,
        user: null,
      };
    }
    default:
      return {...state};
  }
};

const AuthContext = createContext({
  ...initialState,
  method: 'JWT',
  login: (loginInfo: LoginInfo) => {},
  logout: () => {},
  updateUserSession: () => {},
  isManager: false,
});

export const AuthProvider = (
  authProviderProps: AuthProviderProps,
): React.JSX.Element => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const login = async (login: LoginInfo) => {
    const client = await ApiService.client();
    const {data} = await client.post('/auth/login', login);

    const {access_token: accessToken} = data;

    await setSession(accessToken, client);

    const {data: sessionData} = await client.get('/auth/session');
    const {...user} = sessionData;
    dispatch({type: 'LOGIN', payload: {user}});
  };

  const logout = async () => {
    const client = await ApiService.client();

    await setSession('', client);
    dispatch({type: 'LOGOUT'});
  };

  const updateUserSession = async () => {
    try {
      const client = await ApiService.client();
      const accessToken = await AsyncStorage.getItem('accessToken');

      if (accessToken) {
        await setSession(accessToken, client);

        const {data} = await client.get('/auth/session');

        dispatch({
          type: 'INIT',
          payload: {
            isAuthenticated: true,
            user: data,
          },
        });
      } else {
        dispatch({
          type: 'INIT',
          payload: {
            isAuthenticated: false,
            user: null,
          },
        });
      }
    } catch (error) {
      dispatch({
        type: 'INIT',
        payload: {
          isAuthenticated: false,
          user: null,
        },
      });
    }
  };

  useEffect(() => {
    updateUserSession();
  }, []);

  const isManager = state.user?.role === 'MANAGER';

  const contextProps: ContextProps = {
    ...state,
    method: 'JWT',
    login,
    logout,
    updateUserSession,
    isManager,
  };

  return (
    <AuthContext.Provider value={contextProps}>
      {authProviderProps.children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
