import {combineReducers} from 'redux';
import productsReducer from './Product/Product.slice';

const RootReducer = combineReducers({
  products: productsReducer,
});

export default RootReducer;
