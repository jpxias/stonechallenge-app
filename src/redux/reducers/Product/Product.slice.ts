import {Slice, createSlice} from '@reduxjs/toolkit';

import {
  Product,
  createProduct,
  deleteProduct,
  getProduct,
  getProducts,
} from './Product.thunks';

import {Toast} from 'react-native-toast-message/lib/src/Toast';

type ProductState = {
  list: Product[];
  status: string;
  product: Product;
};

const initialState: ProductState = {
  status: 'idle',
  list: [],
  product: {
    id: '',
    name: '',
    serial: '',
  },
};

export const productsSlice: Slice<ProductState> = createSlice({
  name: 'products',
  initialState,
  reducers: {},
  extraReducers: builder =>
    builder
      .addCase(getProducts.pending, state => {
        state.status = 'loading';
      })
      .addCase(getProducts.fulfilled, (state, {payload}) => {
        state.list = payload;
        state.status = 'done';
      })
      .addCase(getProducts.rejected, state => {
        state.status = 'rejected';
      })
      .addCase(getProduct.pending, state => {
        state.status = 'loading';
      })
      .addCase(getProduct.fulfilled, (state, {payload}) => {
        state.product = payload;
        state.status = 'done';
      })
      .addCase(getProduct.rejected, state => {
        state.status = 'rejected';
      })
      .addCase(createProduct.pending, state => {
        state.status = 'loading';
      })
      .addCase(createProduct.fulfilled, (state, {payload}) => {
        state.status = 'done';
        state.list.push(payload);
        Toast.show({
          text1: 'Sucesso',
          text2: 'Ação concluida com sucesso.',
        });
      })
      .addCase(createProduct.rejected, state => {
        state.status = 'rejected';
      })
      .addCase(deleteProduct.pending, state => {
        state.status = 'loading';
      })
      .addCase(deleteProduct.fulfilled, (state, {payload}) => {
        state.status = 'done';
        const index = state.list.findIndex(
          (item: any) => item.id === payload.id,
        );
        if (index !== -1) {
          state.list.splice(index, 1);
        }
        Toast.show({
          text1: 'Sucesso',
          text2: 'Ação concluida com sucesso.',
        });
      })
      .addCase(deleteProduct.rejected, state => {
        state.status = 'rejected';
      }),
});

export const {} = productsSlice.actions;

export default productsSlice.reducer;
