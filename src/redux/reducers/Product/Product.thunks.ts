import {createAsyncThunk} from '@reduxjs/toolkit';
import ApiService from '../../../services/api.service';

export type CreateProductType = {
  product: Product;
  callback: () => void;
};

type DeleteProductType = {
  id: string;
};

type GetProductType = {
  id: string;
};

export type Product = {
  id: string;
  name: string;
  serial: string;
};

export const getProducts = createAsyncThunk<Product[], void>(
  'products/getProducts',
  async (_, {rejectWithValue}) => {
    try {
      const client = await ApiService.client();
      const {data} = await client.get('/products');

      return data;
    } catch (error: any) {
      return rejectWithValue(error.response.data);
    }
  },
);

export const getProduct = createAsyncThunk<Product, GetProductType>(
  'products/getProduct',
  async ({id}, {rejectWithValue}) => {
    try {
      const client = await ApiService.client();
      const {data} = await client.get(`/products/${id}`);

      return data;
    } catch (error: any) {
      return rejectWithValue(error.response.data);
    }
  },
);

export const deleteProduct = createAsyncThunk<any, DeleteProductType>(
  'products/deleteProduct',
  async ({id}, {rejectWithValue}) => {
    try {
      const client = await ApiService.client();
      const res = await client.delete(`/products/${id}`);
      const product: Product = res.data;

      return product;
    } catch (error: any) {
      return rejectWithValue(error.response.data);
    }
  },
);

export const createProduct = createAsyncThunk<any, CreateProductType>(
  'products/createProduct',
  async ({product, callback}, {rejectWithValue}) => {
    try {
      const client = await ApiService.client();
      const {data} = await client.post(`/products`, product);
      if (callback) callback();

      return data;
    } catch (error: any) {
      return rejectWithValue(error.response.data);
    }
  },
);
