import AsyncStorage from '@react-native-async-storage/async-storage';
import clientAxios, {AxiosInstance} from 'axios';
import {API_URL} from '@env';
import {Toast} from 'react-native-toast-message/lib/src/Toast';

export default abstract class ApiService {
  static async client(): Promise<AxiosInstance> {
    const accessToken = await AsyncStorage.getItem('accessToken');

    const client = clientAxios.create({
      baseURL: API_URL,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    client.interceptors.response.use(
      response => response,
      error => {
        Toast.show({
          text1: 'Ocorreu um erro!',
          text2:
            error?.response?.data?.message ||
            'Algo deu errado, tente novamente mais tarde!',
          type: 'error',
        });
        return Promise.reject(
          error?.response?.data?.message ||
            'Algo deu errado, tente novamente mais tarde!',
        );
      },
    );

    return client;
  }
}
