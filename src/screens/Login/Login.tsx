import React, {PropsWithChildren, useEffect} from 'react';
import {Text, TextInput, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {LoginStyles} from './Login.styles';
import {Controller, useForm} from 'react-hook-form';
import useAuth from '../../hooks/useAuth';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {Button} from '@rneui/themed';
import FormItem from '../../components/FormItem/FormItem.component';

type LoginProps = PropsWithChildren<{
  navigation: any;
}>;

function LoginScreen({navigation}: LoginProps): React.JSX.Element {
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
  });

  const auth = useAuth();

  const onSubmit = async (values: any) => {
    try {
      await auth.login({username: values.email, password: values.password});
      navigation.navigate('Home');
      Toast.show({text1: 'Olá!', text2: 'Seja bem vindo ao Ton App!'});
    } catch (error) {
      Toast.show({
        type: 'error',
        text1: 'Credenciais incorretas!',
        text2: 'Confira seu login e tente novamente!',
      });
    }
  };

  useEffect(() => {
    if (auth.isAuthenticated) {
      navigation.navigate('Home');
    }
  }, [auth.isAuthenticated]);

  return (
    <SafeAreaView style={LoginStyles.container}>
      <View style={LoginStyles.insideContainer}>
        <Text style={[LoginStyles.title, LoginStyles.bold]}>Olá,</Text>
        <Text style={[LoginStyles.title]}>Bem-vindo(a) ao Ton</Text>

        <View style={LoginStyles.inputsContainer}>
          <FormItem
            name="email"
            title="Email"
            control={control}
            errors={errors}
            required
            rules={{maxLength: 100}}
          />

          <FormItem
            name="password"
            title="Senha"
            control={control}
            errors={errors}
            required
            rules={{maxLength: 100}}
            password
          />
        </View>

        <Button title="Entrar" onPress={handleSubmit(onSubmit)} />
      </View>
    </SafeAreaView>
  );
}

export default LoginScreen;
