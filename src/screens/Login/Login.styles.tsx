import {StyleSheet} from 'react-native';

export const LoginStyles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },

  insideContainer: {
    textAlign: 'justify',
  },

  title: {
    fontSize: 30,
  },

  bold: {
    fontWeight: 'bold',
    color: 'black',
  },

  error: {
    color: 'red',
  },

  button: {
    backgroundColor: 'green',
    padding: 5,
    color: 'black',
  },

  inputsContainer: {
    paddingVertical: 40,
  },
});
