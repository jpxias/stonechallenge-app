import React, {useEffect} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {styles} from './Home.styles';
import useAuth from '../../hooks/useAuth';
import {useDispatch, useSelector} from 'react-redux';
import {
  Product,
  deleteProduct,
  getProducts,
} from '../../redux/reducers/Product/Product.thunks';
import ListItem from '../../components/ListItem/ListItem.component';
import {AppDispatch, IRootState} from '../../redux/Store';
import LinearGradient from 'react-native-linear-gradient';
import {Button} from '@rneui/themed';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {useNavigation} from '@react-navigation/native';
import {NativeStackParamList} from '../../navigation/Navigation';

function HomeScreen(): React.JSX.Element {
  const auth = useAuth();
  const dispatch = useDispatch<AppDispatch>();
  const products = useSelector<IRootState, Product[]>(
    ({products}) => products.list,
  );

  const navigation =
    useNavigation<NativeStackNavigationProp<NativeStackParamList>>();

  const onDelete = (id: string) => {
    dispatch(deleteProduct({id}));
  };

  useEffect(() => {
    if (auth.isAuthenticated) {
      dispatch(getProducts());
    } else {
      navigation.navigate('Login');
    }
  }, [auth.isAuthenticated]);

  return (
    <SafeAreaView>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#47dc93', '#2F7A11']}>
        <View style={styles.header}>
          <Text style={[styles.title, styles.white]}>
            <Text style={styles.bold}>Olá,</Text> {auth.user?.name}
          </Text>
        </View>
      </LinearGradient>

      <ScrollView style={styles.list}>
        <View style={styles.listHeader}>
          <View>
            <Text style={[styles.bold, styles.black, styles.title]}>
              Disponíveis
            </Text>
            <Text>As melhores taxas do mercado</Text>
          </View>
          {auth.isManager && (
            <Button onPress={() => navigation.navigate('CreateProduct')}>
              Novo
            </Button>
          )}
        </View>

        {products.map(product => (
          <ListItem
            key={product.id}
            item={product}
            showDelete={auth.isManager}
            onDelete={() => onDelete(product.id)}
            onClick={() => navigation.navigate('ViewProduct', {id: product.id})}
          />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
}

export default HomeScreen;
