import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  header: {
    height: 100,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 30,
  },
  bold: {
    fontWeight: 'bold',
  },
  title: {
    fontSize: 20,
  },
  white: {
    color: 'white',
  },
  black: {
    color: 'black',
  },
  listHeader: {
    padding: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  list: {
    backgroundColor: 'white',
    height: '100%',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    marginTop: -30,
    padding: 15,
  },
});
