import {StyleSheet} from 'react-native';

export const ViewProductStyles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    padding: 20,
  },
  image: {
    width: '50%',
    height: 200,
    objectFit: 'contain',
    marginBottom: 20,
  },

  name: {
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
  },
});
