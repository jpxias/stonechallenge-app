import {Text} from '@rneui/themed';
import React, {useEffect} from 'react';
import {View, Image} from 'react-native';
import {ViewProductStyles} from './ViewProduct.styles';
import {useDispatch, useSelector} from 'react-redux';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import Header from '../../components/Header/Header.component';
import {tont1} from '../../../img';
import {AppDispatch, IRootState} from '../../redux/Store';
import {RouteProp} from '@react-navigation/native';
import {Product, getProduct} from '../../redux/reducers/Product/Product.thunks';

type ViewProductScreenProps = {
  navigation: NativeStackNavigationProp<{}>;
  route: RouteProp<{}>;
};

const ViewProductScreen: React.FC<ViewProductScreenProps> = ({
  navigation,
  route,
}) => {
  const product = useSelector<IRootState, Product>(
    ({products}) => products.product,
  );
  const {id} = route.params;

  const dispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    dispatch(getProduct({id}));
  }, []);

  return (
    <View>
      <Header title="Visualizar Produto" goBack={navigation.goBack} />
      <View style={ViewProductStyles.container}>
        <Image source={tont1} style={ViewProductStyles.image} />
        <Text style={ViewProductStyles.name}>{product.name}</Text>
        <Text>{product.serial}</Text>
      </View>
    </View>
  );
};

export default ViewProductScreen;
