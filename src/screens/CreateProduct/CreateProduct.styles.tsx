import {StyleSheet} from 'react-native';

export const CreateProductStyles = StyleSheet.create({
  error: {
    color: 'red',
  },

  buttons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  container: {
    display: 'flex',
    gap: 20,
    padding: 20,
  },
});
