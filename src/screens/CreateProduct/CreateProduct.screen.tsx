import {Button} from '@rneui/themed';
import React from 'react';
import {useForm} from 'react-hook-form';
import {View} from 'react-native';
import {CreateProductStyles} from './CreateProduct.styles';
import {useDispatch, useSelector} from 'react-redux';
import {createProduct} from '../../redux/reducers/Product/Product.thunks';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import Header from '../../components/Header/Header.component';
import {AppDispatch, IRootState} from '../../redux/Store';
import {useNavigation} from '@react-navigation/native';
import {NativeStackParamList} from '../../navigation/Navigation';
import FormItem from '../../components/FormItem/FormItem.component';

const CreateProductScreen = () => {
  const dispatch = useDispatch<AppDispatch>();
  const navigation =
    useNavigation<NativeStackNavigationProp<NativeStackParamList>>();

  const status = useSelector<IRootState, string>(
    ({products}) => products.status,
  );

  const isLoading = status === 'loading';

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      name: '',
      serial: '',
    },
  });

  const onSubmit = (values: any) => {
    dispatch(
      createProduct({
        product: values,
        callback: () => {
          navigation.goBack();
        },
      }),
    );
  };

  const onCancel = () => {
    navigation.goBack();
  };

  return (
    <View>
      <Header title="Novo Produto" goBack={navigation.goBack} />
      <View style={CreateProductStyles.container}>
        <FormItem
          control={control}
          required
          name="name"
          title="Nome"
          errors={errors}
          rules={{maxLength: 100}}
        />

        <FormItem
          control={control}
          required
          name="serial"
          title="Serial"
          errors={errors}
          rules={{maxLength: 100}}
        />

        <View style={CreateProductStyles.buttons}>
          <Button
            title="Confirmar"
            onPress={handleSubmit(onSubmit)}
            loading={isLoading}
          />
          <Button title="Cancelar" onPress={onCancel} loading={isLoading} />
        </View>
      </View>
    </View>
  );
};

export default CreateProductScreen;
