import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import LoginScreen from '../screens/Login/Login';
import HomeScreen from '../screens/Home/Home';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import CreateProductScreen from '../screens/CreateProduct/CreateProduct.screen';
import ViewProductScreen from '../screens/ViewProduct/ViewProduct.screen';

const Stack = createNativeStackNavigator();

export type NativeStackParamList = {
  Login: undefined;
  Home: undefined;
  CreateProduct: undefined;
  ViewProduct: {id: string};
};

const Navigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{header: () => null}}>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{title: 'Welcome'}}
        />
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="CreateProduct" component={CreateProductScreen} />
        <Stack.Screen name="ViewProduct" component={ViewProductScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
