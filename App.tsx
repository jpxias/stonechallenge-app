import React from 'react';
import Navigation from './src/navigation/Navigation';
import {Provider} from 'react-redux';
import store from './src/redux/Store';
import {AuthProvider} from './src/context/AuthContext';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ThemeProvider, createTheme} from '@rneui/themed';

const theme = createTheme({
  lightColors: {
    primary: '#4cbb17',
  },
  darkColors: {
    primary: '#2F7A11',
  },
  mode: 'light',
});

function App(): React.JSX.Element {
  return (
    <SafeAreaProvider>
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <AuthProvider>
            <Navigation />
          </AuthProvider>
        </Provider>
        <Toast position="bottom" />
      </ThemeProvider>
    </SafeAreaProvider>
  );
}

export default App;
